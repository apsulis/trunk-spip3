<?php   
    require_once 'php/classes/mobile_detect.php';
    $detect = new Mobile_Detect;
    $isDev = true;
?>

<!doctype html>
<!--[if lte IE 7]> <html lang="fr" class="ie7 old-ie no-js"> <![endif]-->
<!--[if (IE 8)&!(IEMobile)]> <html lang="fr" class="old-ie ie8 no-js"> <![endif]-->
<!--[if (gt IE 8)&!(IEMobile)]><!--> <html lang="fr" class="no-js" > <!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>Project Name</title>
    <meta name="description" content="Project description" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="shortcut icon" href="theme/images/icons/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="theme/images/icons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="theme/images/icons/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="theme/images/icons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="theme/images/icons/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="theme/images/icons/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="theme/images/icons/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="theme/images/icons/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="theme/images/icons/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="theme/images/icons/apple-touch-icon-180x180.png" />
    <?php 
        if(!$isDev){
            echo '
                <link rel="stylesheet" href="theme/css/global.min.css"/>
            ';
        }else{
            echo '
                <link rel="stylesheet" href="bundles/bootstrap-select/dist/css/bootstrap-select.css"/>
                <link rel="stylesheet" href="bundles/wow/css/libs/animate.css"/>
                <link rel="stylesheet" href="bundles/magnific-popup/dist/magnific-popup.css">
                <link rel="stylesheet" href="theme/css/global.css"/>
            ';
        }
    ?>
</head>

<body role="document">
    
    <div id="anchor-top">
    </div><!-- #anchor-top -->
    
    <div id="ie-fallback">    
        <a href="http://windows.microsoft.com/fr-fr/internet-explorer/download-ie" title="Mettre à jour Internet Explorer" id="ie-box"></a>
    </div><!-- #ie-fallback -->

    <ul id="skip-links">
        <li>
            <a href="#nav-main" data-anchor="menu">Menu de navigation</a>
        </li>
        <li>
            <a href="#main" data-anchor="main">Contenu principal</a>
        </li>
    </ul><!-- #skip-links -->

    <div class="ie-wp">
        <header id="header" class="bg-wh-a hidden-print">    
            <button id="nav-mobile-trigger" type="button" aria-haspopup="true" aria-expanded="true">
                <span class="btn-mobile-hamburger">
                    <span></span> <span></span> <span></span><span></span>
                </span><!-- #btn-hamburger -->
            </button>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul id="nav-main" class="d-inbl">
                            <li class="item" data-navsub>
                                <a href="#" class="link" title=""><span class="txt">Item 1</span></a>
                                <ul class="nav-sub">
                                    <li class="item">
                                        <a href="#" class="link"><span class="txt">Sub-item 1</span></a>
                                    </li>
                                    <li class="item active">
                                        <a href="#" class="link"><span class="txt">Sub-item 2</span></a>
                                    </li>
                                    <li class="item nav-sub-close">
                                        <a href="#" class="link">Fermer</a>
                                    </li>
                                </ul><!-- .sub-menu -->
                            </li><!-- .item -->
                            <li class="item" data-navsub>
                                <a href="#" class="link" title=""><span class="txt">Item 2</span></a>
                                <ul class="nav-sub">
                                    <li class="item">
                                        <a href="#" class="link"><span class="txt">Sub-item 1</span></a>
                                    </li>
                                    <li class="item active">
                                        <a href="#" class="link"><span class="txt">Sub-item 2</span></a>
                                    </li>
                                    <li class="item nav-sub-close">
                                        <a href="#" class="link">Fermer</a>
                                    </li>
                                </ul><!-- .sub-menu -->
                            </li><!-- .item -->
                             <!-- Component :: Trigger Modal  -->
                            <li class="item">
                                <a href="modal.php" class="link trigger-modal" title=""><span class="txt">Modal Launch</span></a>
                            </li><!-- .item -->
                        </ul><!-- #nav-main -->
                    </div><!-- .col-md-12 -->
                </div><!-- .row -->
            </div><!-- .container -->
         </header><!-- #header-->


