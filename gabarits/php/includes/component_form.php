<!-- Component :: Form -->
<form action="#" class="form-validator" data-form="example" autocomplete="off" role="form">
    <!-- ALERT PHP SUCCESS -->
    <!-- <div class="alert alert-success" role="alert">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, atque</p>
        </div>-->
    <!-- ALERT PHP INVALID -->
    <!--<div class="alert alert-danger" role="alert">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, atque</p>
        <ul class="mt5">
            <li>&bull; Champs XXX</li>
            <li>&bull; Champs XXX</li>
            <li>&bull; Champs XXX</li>
        </ul>
    </div> -->
    <div>
        <input type="text" name="text" id="text" class="form-control" placeholder="Texte" >
    </div>
    <div>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email" >
    </div>
    <div>
        <select id="select" name="select" class="selectpicker" data-container="body" data-errorSelect="yes">
            <option value="0" selected="selected">Toutes les catégories</option>
            <option value="1" >Compte-rendu</option>                   
            <option value="2" >Conseil d’administration</option>                   
            <option value="3" >Procès-verbal AG</option>                   
        </select>  
    </div> 
    <div>
        <input type="file" name="file" id="file" class="inputfile" data-multiple-caption="{count} files selected" data-errorFile="yes" />
        <label for="file"><span>Séléctionner un fichier&hellip;</span></label>
    </div>
    <div>
        <input type="text" name="phone" id="phone" class="form-control" placeholder="Téléphone Fixe" >
    </div>
    <div>
        <input type="text" name="mobilePhone" id="mobilePhone" class="form-control" placeholder="Téléphone Mobile" >
    </div>
    <div>
        <input type="password" name="password" id="password" class="form-control" placeholder="Mot de passe" >
    </div>
    <div>
        <input type="password" name="passwordConfirm" id="passwordConfirm" class="form-control" placeholder="Confirmer champs Mot de passe" >
    </div>

    <div class="checkradio-wp">
        <input type="checkbox" name="checkbox" id="checkbox" data-errorCheckbox="yes"> 
        <label for="checkbox">Chekbox</label>
    </div><!-- .checkradio-wp -->  
    <div class="checkradio-wp">
        <input type="radio"  name="radio-array" id="radio-1" checked > 
        <label for="radio-1">Radio 1 </label>
    </div><!-- .checkradio-wp -->  
    <div class="checkradio-wp">
        <input type="radio"  name="radio-array" id="radio-2" > 
        <label for="radio-2">Radio 2 </label>
    </div><!-- .checkradio-wp -->  
    <button type="submit" class="btn">Valider</button>
</form><!-- .form-validator -->
<!-- End Component :: Form -->

