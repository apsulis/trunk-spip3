<!-- Component :: Carousel Horizontal  -->
<div class="carousel-example-wp carousel-horizontal-wp">
    <ul class="carousel-horizontal" id="carousel-example-1" data-timer="5000" data-auto="true"  data-pagination="carousel-example-pagination-1" data-itemsDesktop="6" data-itemsTablet="3" data-itemsPhone="2" data-itemScroll="2">
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit.">
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
        <li class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." >
                <span class="sprite zoom-a"></span>
                <img src="http://placehold.it/350x150" class="img-responsive" alt="#">
            </a>
        </li>
    </ul><!-- .carousel-horizontal.row -->
    <div class="carousel-horizontal-nav hidden-js hidden-print">
        <div id="carousel-example-pagination-1" class="carousel-pagination ta-c mt-20"></div>
    </div><!-- .carousel-horizontal-nav  -->                        
</div><!-- .carousel-publications -->
<!-- End Component :: Carousel Horizontal  -->

<br><br><br><br>

<!-- Component :: Carousel Full Width  -->
<div id="carousel-full-width-wp">
    <div id="carousel-full-width-pagination" class="carousel-pagination hidden-js hidden-print"></div>
    <button id="carousel-full-width-prev" class="hidden-js hidden-print">Précédent</button>
    <button id="carousel-full-width-next" class="hidden-js hidden-print">suivant</button>
    <ul id="carousel-full-width">
        <li class="item">
            <a href="#" class="link" style="background-image: url('http://lorempixel.com/1200/515/city/');" title="#">
                <article class="container">
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, consequatur?
                    </div><!-- .content -->
                    <img src="http://lorempixel.com/1200/515/city/" class="hidden" alt="#">
                </article><!-- .container -->
            </a><!-- .link -->
        </li><!-- .item -->            
        <li class="item">
            <a href="#" class="link" style="background-image: url('http://lorempixel.com/1200/515/city/');" title="#">
                <article class="container">
                    <div class="content">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, consequatur?
                    </div><!-- .content -->
                    <img src="http://lorempixel.com/1200/515/city/" class="hidden" alt="#">
                </article><!-- .container -->
            </a><!-- .link -->
        </li><!-- .item -->            
    </ul><!-- #carousel-full-width -->
</div><!-- #carousel-full-width-wp -->
<!-- End Component :: Carousel Full Width  -->

