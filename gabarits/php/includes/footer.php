	<footer id="footer" class="hidden-print" role="contentinfo">
	</footer><!-- #footer -->

	<div id="modal" class="modal fade"  tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			</div><!-- .modal-content -->
		</div><!-- .modal-dialog -->
	</div><!-- .modal -->

	<div id="modal-loader">
        <div class="spinner-wp spinner-circle"></div>
	</div><!-- #modal-loader -->

</div><!-- .ie-wp -->

    <?php 

        // Environment :: Production
        if(!$isDev){
            echo '
                <script src="theme/javascript/global.min.js"></script>
            ';

        // Environment :: Development
        }else{
            echo '
            	<script src="bundles/jquery/dist/jquery.min.js"></script>
            	<script src="bundles/jquery-placeholder/jquery.placeholder.min.js"></script>
            	<script src="bundles/jquery.scrollTo/jquery.scrollTo.min.js"></script>
                <script src="bundles/jquery.localScroll/jquery.localScroll.min.js"></script>
            	<script src="bundles/jquery.carouFredSel-6.2.1/index.js"></script>
            	<script src="bundles/jquery-touchswipe/jquery.touchSwipe.min.js"></script>
            	<script src="bundles/jquery-validation/dist/jquery.validate.min.js"></script>
            	<script src="bundles/jquery-validation/dist/additional-methods.min.js"></script>
                <script src="bundles/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
                <script src="bundles/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js"></script>
                <script src="bundles/bootstrap-sass/assets/javascripts/bootstrap/modal.js"></script>
                <script src="bundles/bootstrap-select/js/bootstrap-select.js"></script>
            	<script src="bundles/wow/dist/wow.min.js"></script>
				<script src="theme/javascript/global.js"></script>
                <!--<script src="//localhost:35729/livereload.js"></script>-->
            ';
        }

        // Tablet :: Nav Sub on Touch Event
        if( $detect->isTablet() ){
            echo '<script>f_headerSubNavTablet();</script>';
        }        
    ?>


</body>
</html>