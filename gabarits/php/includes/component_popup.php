<!-- Component :: Magnific Pop Up -->
<ul class="row magnific-popup">
    <li class="col-md-2 col-sm-2 col-xs-2">
        <a href="https://www.youtube.com/watch?v=PAuN_oF5KS4" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." class="item mfp-iframe" data-effect="mfp-zoom-out">
            <span class="sprite zoom-a"></span>
            <img src=" http://placehold.it/350x150" class="img-responsive" alt="#">
        </a>
    </li>
    <li class="col-md-2 col-sm-2 col-xs-2">
        <a href="http://placehold.it/350x150" title="Lorem ipsum dolor sit amet, consectetur adipisicing elit." class="item " data-effect="mfp-zoom-out">
            <span class="sprite zoom-a"></span>
            <img src=" http://placehold.it/350x150" class="img-responsive" alt="#">
        </a>
    </li>
</ul>
<!-- End Component :: Magnific Pop Up -->
