/*!
* Project NAME - @author: Jérémie Maquet || www.jmaquet.fr/ - Agence NAME 
*/

// Internet Explorer Mobile :: Detect Variable
var isIEmobile = false;
if(navigator.userAgent.match(/iemobile/i)){                                      
    var isIEmobile = true;
}

// Internet Explorer Mobile :: Windows Phone 8 Fixes
function f_ieMobileFixes(){
    (function() {if (navigator.userAgent.match(/IEMobile\/10\.0/)) {var msViewportStyle = document.createElement("style");msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));document.getElementsByTagName("head")[0].appendChild(msViewportStyle);}})();
}

// Header :: Skip Link 
function f_headerSkipLinks(){
    $('#skip-links a').on('click',function(){
        anchor = $(this).attr('data-anchor');
        if(anchor=='menu'){
            $('#nav-main a:first').focus();
        }
        if(anchor=='main'){
            $('#main a:first, #main button:first').focus();
        }
    });
}

// Header :: Sub Navigation Focus
function f_headerSubNavFocus (){

    $('#nav-main li[data-navsub] > a').on('focus',function(){
        $('#nav-main li[data-navsub]').removeClass('focus');
        var navSub = $(this).parent();
        navSub.addClass('focus');
    });
     $('#nav-main li[data-navsub]').on("mouseover", function () {
        $('#nav-main .item').removeClass('focus');
     });
}

// Header :: Sub Navigation Tablet Touch
function f_headerSubNavTablet(){

    // step 0 : Ajout de la classe 'tablet-on' pour afficher bouton de fermeture
    $('body').addClass('tablet-on');

    // step 1 : On masque chaque big menu en stockant sa hauteur initiale
    $('.nav-sub').css({visibility : 'visible', height : 'inherit', opacity : 1});
    $('.nav-sub').each(function(){
        var subMenuHeight = $(this).innerHeight();
        $(this).attr('data-height',subMenuHeight);
        $(this).css({visibility : 'hidden', height : '1px', opacity : 0});
    });

    // step 2 : Ouverture du big menu au clic 
    $('#nav-main li[data-navsub] > a').on('click',function(event){
        var subMenu = $(this).next('.nav-sub');
        f_menuReset();
        if((subMenu.css('visibility')!='visible')){
            $(this).addClass('active');
            subMenu.css({visibility : 'visible', height : subMenu.attr('data-height'), opacity : 1});
            return false;
        }
    });

    // step 3 : Fermeture du big menu au clic bouton fermer 
    function f_menuReset(){
        $('#nav-main .link').removeClass('active');
        $('#nav-main .nav-sub').css({visibility : 'hidden', height : '1px', opacity : 0});
    }
     $('#nav-main .nav-sub-close').on('click',function(){
        f_menuReset();
        return false;
    });
}

// Header :: Mobile
function f_headerMobile (){

    $('#nav-mobile-trigger').on('click',function(){ 
        $('body').toggleClass('menu-open');
        $('#nav-main').slideToggle();
    });

}

// Header :: Up / Down Scrolling
function f_headerUpDown(){

    var viewport = $(window).width();
    if(viewport>991){
        value = parseInt(130);
    }else{
        value = parseInt(150);
    }

    var position = $(window).scrollTop();

    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if(($(window).scrollTop())>value){
            if (scroll > position) {
                $('body').addClass('header-up').removeClass('header-down');
            }else{
                $('body').addClass('header-down').removeClass('header-up');
            }
            position = scroll;
        }else{
                $('body').removeClass('header-down header-up');
        }
    });
}

// Caroufredesel :: Horizontal Generic
function f_carouselHorizontal(){

    $('.carousel-horizontal').each(function(i) {
        //Fonction Viewport pour déterminer le nombre d'items à afficher
        function f_viewport(){
            var viewport  = $(window).width();
            if(viewport > 991 ){
                itemsNumber = itemsDesktop;
            }
            if(viewport< 991 && viewport > 767){
                itemsNumber = itemsTablet;
            }if(viewport < 767){
                itemsNumber = itemsPhone;
            }
        }
        // Intialisation des variables
        var id            = $(this).attr('id');
        var prev          = $(this).attr('data-prev');
        var next          = $(this).attr('data-next');
        var pagination    = $(this).attr('data-pagination');
        var fx            = $(this).attr('data-fx');
        var auto          = false;
        if ($(this).attr('data-auto')) {
            auto = true;
        }
        var timer         = parseInt($(this).attr('data-timer'));
        var itemsDesktop  = parseInt($(this).attr('data-itemsDesktop'));
        var itemsTablet   = parseInt($(this).attr('data-itemsTablet'));
        var itemsPhone    = parseInt($(this).attr('data-itemsPhone'));
        var itemScroll    = parseInt($(this).attr('data-itemScroll'));

        // Calcul du viewport
        f_viewport();

        // Lancement carousel
        $('#'+id).carouFredSel({
            auto                : auto,
            infinite            : true,
            circular            : true,
            responsive          : true,
            height              : "variable",
            items               : itemsNumber,
            direction           : "left",
            prev                : "#"+prev ,
            next                : "#"+next,
            scroll : {
                fx              : fx,
                height          : "variable",
                items           : itemScroll,
                easing          : "swing",
                duration        : 800,
                timeoutDuration : timer,
                pauseOnHover    : true
            },
            pagination : {
                container       : "#"+pagination,
                deviation       : 0
            },
            onBefore : function(data){
                visibleItem = data.items.visible;
                visibleItem.addClass('active');
                height = visibleItem.height();
                $('#'+id+' .caroufredsel_wrapper').css('height',height);
            },
            onAfter : function(data){
                visibleItem = data.items.visible;
                visibleItem.addClass('active');
            },
            //Au resize on recalcule le viewport pour adapter le nombre d'items
            onCreate: function (data) {
                function f_visible(){
                    visibleItem = $('#'+id+' .active');
                    height = visibleItem.height();
                    $('#'+id+' .caroufredsel_wrapper').css('height',height);
                }
                $(window).on('resize', function () {
                    f_viewport();
                   // console.log(itemsNumber)
                    $('#'+id).trigger("configuration", {
                        items: itemsNumber
                    });
                    f_visible();
                });
               $(window).trigger('resize');
             }
        });

        $('#'+id).swipe({
            excludedElements: "button, input, select, textarea, .noSwipe",
            swipeLeft: function() {
                    $('#'+id).trigger('next');
            },
            swipeRight: function() {
                    $('#'+id).trigger('prev');
            }
        });

    });
}

// Caroufredesel :: Full Width
function f_carouselFullWidth(){

    function highlight( items ) {
        items.filter(":eq(0)").addClass("active");
    }

    function unhighlight( items ) {
        items.filter(":eq(0)").addClass("active");
        items.removeClass("active");
    }

    function f_carouselFullWidthLaunch(){
        $('#carousel-full-width').carouFredSel({
            responsive          : true,
            items               : 1,
            height              : height,
            width               : '100%',
            auto                : false,
            prev                : "#carousel-full-width-prev",
            next                : "#carousel-full-width-next",
            scroll     : {
                fx              : 'cover',
                items           : 1,
                height          : height,
                width           : '100%',
                easing          : 'quadratic',
                pauseOnHover    : true,
                duration        : 300,
                timeoutDuration : 5000,
                onBefore: function( data ) {
                    unhighlight(data.items.old);
                },
                onAfter : function( data ) {
                    highlight(data.items.visible);
                }
            },
            pagination : {
                container       : "#carousel-full-width-pagination",
                deviation       : 0
            },
            onCreate: function (data) {
                data.items.filter(":eq(0)").addClass("active");
                $(window).on('resize', function () {
                      clearTimeout(resizeEnd);
                      resizeEnd = setTimeout(f_resizeEnd, 100);
                });
             }
        });

        $('#carousel-full-width').swipe({
            excludedElements: "button, input, select, textarea, .noSwipe",
            swipeLeft: function() {
                $('#carousel-full-width').trigger('next');
            },
            swipeRight: function() {
                 $('#carousel-full-width').trigger('prev');
            }
        });
    }


    function f_viewport(){
        var viewport = $(window).width();
        if(viewport > 991){
            height = 800;
        }else if(viewport > 767) {
            height = 400;
        }else{
            height = 200;
        }
        $("#carousel-full-width").trigger("configuration", ["height", height]);
        $("#carousel-full-width").trigger("configuration", ["scroll.height", height]);
    }

    function f_resizeEnd(){
        f_viewport();
    }

    var resizeEnd;
    f_viewport();
    f_carouselFullWidthLaunch();

}

// Modal :: Initialisation 
function f_modal(){

    function f_modalLoad(href){
        //On vide le contenu de la modal
        $('#modal .modal-body').html();
        //On affiche le loader
        $('body').addClass('modal-loading');
        $('#modal .modal-content').load(href+' #modal-content',function(response, status, xhr){ 
            //Si erreur on affiche une alerte javascript
            if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                alert( msg + xhr.status + " " + xhr.statusText );
            }else{
                $('#modal').modal();
                $('input, textarea').placeholder();
                f_formValidator();
                $('.trigger-modal-inner').on('click',function(){
                    var href = $(this).attr('href'); 
                    f_modalLoad(href);
                    return false;
                });
            }
            //On masque le loader
            $('body').removeClass('modal-loading');
        });
    }

    $('.trigger-modal').on('click',function(){
        var href = $(this).attr('href'); 
        f_modalLoad(href);
        return false;
    });

}

// Magnific Pop Up :: Intialisation
function f_magnificPopup(){
    $('.magnific-popup').each(function(i) {
        $(this).magnificPopup({
            preloader: true,
            removalDelay: 500, //delay removal
            delegate: 'a',
            type: 'image',
            gallery:{
                enabled:true
            },
            callbacks: {
                beforeOpen: function() {
                   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                   this.st.mainClass = this.st.el.attr('data-effect');
                }
            }

        });
    });
}

// Images Preloader :: Intialisation
function  f_preloadImagesLogic(images, text ,callback) {

    var count = images.length;
    if(count === 0) {
        callback();
    }
    var loaded = 0;
    $(images).each(function() {
        $('<img>').attr('src', this).load(function() {
            loaded++;
            if (loaded === count) {
                callback();
            }
        });
    });
}

// Images Preloader :: Action
function f_preloadImages(){

    // On rassemble toutes les image dans un tableau
    var imgArray = Array();
    $('body img').each(function() {
        imgArray.push($(this).attr('src'));
    });
    var imgArrayLength = imgArray.length;

    // Compete event
    f_preloadImagesLogic(imgArray,'', function() {
        alert('ok');
    }); 

}

// Form Input File :: Initialisation
function f_formInputFile(){
    $('.inputfile').each( function(){
        var $input   = $( this ),
            $label   = $input.next( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e ){
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $label.find( 'span' ).html( fileName );
            else
                $label.html( labelVal );
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });
}

// Form Validator :: Initialisation
function f_formValidator(){

    // Additionnal Method :: Phone FR
    $.validator.addMethod("mobilephoneFR", function(phone_number, element) {
        phone_number = phone_number.replace(/\(|\)|\s+|-/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^0[67]([-\/. ]?[0-9]{2}){4}$/);
    }, "Veuillez indiquer un numéro de téléphone mobile valide");

    // Additionnal Method :: Mobile Phone FR
    $.validator.addMethod("phoneFR", function(phone_number, element) {
        phone_number = phone_number.replace(/\(|\)|\s+|-/g, "");
        return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^0[0-9]([-\/. ]?[0-9]{2}){4}$/);
    }, "Veuillez indiquer un numéro de téléphone valide");

    jQuery.validator.setDefaults({
        debug: false,
        success: function(label) {
            label.addClass("valid").text("Ok");
        },
        errorPlacement: function(error, element) {
            if (element.attr("data-errorCheckbox") || element.attr("data-errorFile") == "yes") {
                newElem = element.parent();
                error.insertAfter(newElem);
            }else if(element.attr("data-errorSelect")){
                newElem = element.parent().find('.bootstrap-select');
                error.insertAfter(newElem);
            }else if(element.attr("data-errorFile")){
                newElem = element.parent().find('.inputfile');
                error.insertAfter(newElem);
            }else {
                error.insertAfter(element);
            }         
        },
        ignore: ':hidden:not(".selectpicker")',

    });

    $('.form-validator').each(function(i) {
        var form = $(this).attr('data-form');
        //Formulaire :: Adhésion
        if(form=='example'){
            $(this).validate({ 
                rules: {
                    text               :     { required: true},
                    email              :     { required: true, email: true},
                    select             :     { required: true, min:1 },
                    file               :     { required: true, extension: "jpg|jpeg|gif|png|pdf"},
                    mobilePhone        :     { required: { depends: function(element){ return $("#phone").is(":blank"); } }, mobilephoneFR: true },
                    phone              :     { required: { depends: function(element){ return $("#mobilePhone").is(":blank"); } },  phoneFR: true },
                    password           :     { required: true,  minlength: 5 },
                    passwordConfirm    :     { required: true,  equalTo : "#mdp"},
                    checkbox           :     { required: true},
                },
                messages: {
                    text               :     'Le champs "text" est requis',
                    email              :     'Le champs "Email" est requis',
                    select             :     'Veuillez indiquer un champs de sélection',
                    file               :     'Veuillez uploader un fichier de type .jpg | .pdf | etc.',
                    mobilePhone        :     'Le champs "Téléphone" ou "portable" est requis',
                    phone              :     'Le champs "Téléphone" ou "portable" est requis',
                    password           :     'Le champs "Mot de Passe" est requis',
                    passwordConfirm    :     'Veuillez répéter Le champs "Mot de Passe"',
                    checkbox           :     'Veuillez cocher la case xxx'
                }                
            });           
        }
    });
}

// WOW :: Initialisation
function f_wow(){
    new WOW().init();
}

/* Fonctions Générales */
function myInitPages() {
    $('html').removeClass('no-js').addClass('hasJS');
    $('input, textarea').placeholder();
    if(!isIEmobile){  
        $.localScroll(); 
    }
    f_ieMobileFixes();
    f_modal();
    f_wow();
    f_headerSkipLinks();
    f_headerMobile();
    f_headerUpDown();
    f_headerSubNavFocus();
    //f_preloadImages();
    $(window).on('resize',function(){   
        f_headerUpDown();
    });
}

/* Chargement des fonctions au démarrage */
$(document).ready(function() { 
    myInitPages();
    // Function :: Carousel Horizontal 
    if($('.carousel-horizontal').length)        { f_carouselHorizontal(); }
    // Function :: Carousel Full Width 
    if($('#carousel-full-width').length)        { f_carouselFullWidth(); }
    // Function :: Validator Form 
    if($('.form-validator').length)             { f_formValidator(); }
    // Function :: Magnific Pop Up 
    if($('.magnific-popup').length)             { f_magnificPopup(); }
    // Function :: Input File
    if($('.inputfile').length)                  { f_formInputFile(); }
});

