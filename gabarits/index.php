<?php include('php/includes/header.php') ?>

	<main id="main" role="main">
		<div class="container">
            <div class="row">
                <section class="col-md-8">
                </section><!-- .col-md-8 -->
                <aside class="col-md-4" role="complementary">
                </aside><!-- .col-md-4 -->
            </div><!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    <?php include('php/includes/component_form.php') ?>
                    <?php include('php/includes/component_popup.php') ?>
                    <?php include('php/includes/component-carousel.php') ?>
                </div><!-- .col-md-12 -->
            </div><!-- .row -->
		</div><!-- .container -->
	</main><!-- #main-->

<?php include('php/includes/footer.php') ?>
