<?php include('php/includes/header.php') ?>

	<main id="main" role="main">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="modal-content">
						<div class="modal-header">
							<h1 class="">Titre modal</h1>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div><!-- .modal-header -->
						<div class="modal-body">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, pariatur, minima? Saepe voluptatum modi natus at sequi velit pariatur dicta.</p>
						</div><!-- .modal-body -->
						<div class="modal-footer">
							<a href="modal.php" class="btn trigger-modal-inner">Modal inside modal Btn</a>
						</div><!-- .modal-footer -->
			    	</div><!-- #modal-content-->
				</div><!-- .col-md-12 -->
      		</div><!-- .row -->
      	</div><!-- .container -->
	</main><!-- #main-->

<?php include('php/includes/footer.php') ?>

