module.exports = function(grunt){

	// Plugin pour loader automatiquement les dépendances à partir du fichier package.json
	require('load-grunt-tasks')(grunt);

	// Initialisation des tâches
	grunt.initConfig({

		// JSHINT : Tâche pour Vérifier syntaxe JS
  		jshint: {
    		all: ['theme/javascript/global.js']
  		},

		// UGLIFY : Tâche pour minifier le Javascript
	  	uglify: {
    		dist: {
    			// On renseigne les fichier à minifier - Attention à l'odre, grunt va minifier selon l'ordre indiqué ci-dessous
      			files: {
        			'theme/javascript/global.min.js': ['bundles/jquery/dist/jquery.min.js','bundles/jquery-placeholder/jquery.placeholder.min.js','bundles/jquery.scrollTo/jquery.scrollTo.min.js','bundles/jquery.localScroll/jquery.localScroll.min.js','bundles/jquery.carouFredSel-6.2.1/index.js','bundles/jquery-touchswipe/jquery.touchSwipe.min.js','bundles/jquery-validation/dist/jquery.validate.min.js','bundles/jquery-validation/dist/additional-methods.min.js','bundles/magnific-popup/dist/jquery.magnific-popup.min.js','bundles/bootstrap-sass/assets/javascripts/bootstrap/modal.js','bundles/bootstrap-select/js/bootstrap-select.js','bundles/wow/dist/wow.min.js','theme/javascript/global.js']
      			}
    		}
  		},

  		// SASS : Tâche pour convertir les fichiers SCSS EN CSS
		sass: {                           
		    dist: {                         
		      	files: {                        
		        	'theme/css/global.css': 'theme/sass/global.scss'      
		      	}
		    }
		},

		// CSSMIN : Tâche pour minifier le CSS
  		cssmin: {
  			dist: {
    			// On renseigne les fichier à minifier - Attention à l'odre, grunt va minifier selon l'ordre indiqué ci-dessous
    			files: {
      				'theme/css/global.min.css': ['bundles/bootstrap-select/dist/css/bootstrap-select.css','bundles/magnific-popup/dist/magnific-popup.css','bundles/wow/css/libs/animate.css','theme/css/global.css']
    			}
  			}
		},

		// IMAGIN : Tâche pour minifier les images
		imagemin: {                          
		    images: {                         
		     	files: [{
		        	expand: true,                  
		        	cwd: 'theme/images/',                   
		        	// Parcourier les sous-dossiers
		        	src: ['**/*.{png,jpg,gif}'],   
		        	dest: 'theme/images/'                 
		      	}]
		    },
		    tmp: {                         
		     	files: [{
		        	expand: true,                  
		        	cwd: 'uploads/',                   
		        	src: ['*.{png,jpg,gif}'],   
		        	dest: 'theme/tmp/'                 
		      	}]
		    }
		 }, 

		// WATCH : Tâche pour automatiser le lancement des tâches lors d'une MAJ d'un des fichiers listés
		watch: {
			// js: {
			// 	files: ['theme/js/*.js','!theme/js/global.min.js'],
			// 	tasks: ['jshint', 'uglify'],
			// 	options: { spawn : false}
			// },
			css: {
				files: ['theme/sass/**/*.scss'],
				tasks: ['sass'],
				options: { 
					spawn : false,
					livereload: true
				}
			}
		}


	});

	// Instruction pour Mettre en production
	grunt.registerTask('production', ['jshint', 'uglify', 'sass', 'cssmin', 'imagemin']);

}