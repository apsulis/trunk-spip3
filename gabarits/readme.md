<h1>1) Install Sass and Bourbon</h1>
<p>Install Ruby, then Sass GEM : <a href="http://sass-lang.com/install"> More info </a></p>
<p>Install Bourbon Gem : <code>gem install bourbon</code></p>	
<h1>2) Install and initialize Grunt</h1>
<p>Grunt require <a href="https://nodejs.org/en/">NodeJS</a></p>
<p>Install Grunt on the system : <code>npm install -g grunt-cli</code></p>
<p>Then install assets included in package.json : <code>npm install</code></p>
<h1>3) Install and initialize Bower</h1>
<p>(Bower require NodeJS)</a></p>
<p>Install Bower on the system : <code>npm install -g bower</code></p>
<p>Then install assets included in file bower.json : <code>bower install</code></p>
<h1>4) Convert SCSS to CSS with Grunt</h1>
<p>Launch the WATCH task to compiled CSS automaticaly in each update of SCSS files : <code>grunt watch</code></p>
<h1>5) Minify CSS/JS/IMAGES</h1>
<p>Launch the PRODUCTION task : <code>grunt production</code></p>
<p>Then switch variable environment : <code>$isDev = false</code></p>
