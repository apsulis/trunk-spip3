<?php
define('_DUREE_CACHE_DEFAUT', 24*3600*30);

// Passer le cache à 100Mo
// $quota_cache=1000;
// $GLOBALS['quota_cache'] = 100;

// Evite la lame du Couteau Suisse
$GLOBALS['table_des_traitements']['TITRE'][]= 'trim(ptobr(propre(supprimer_numero(%s))))';
$GLOBALS['table_des_traitements']['SURTITRE'][]= 'trim(ptobr(propre(supprimer_numero(%s))))';
// $GLOBALS['table_des_traitements']['TEXTE'][]= 'clear_both(propre(%s))';

// Activer le plugin nospam sur des formulaires supplémentaires
// $GLOBALS['formulaires_no_spam'][] = 'soumission_annuaire';

/* Les librairies UI supplémentaires */
// $GLOBALS['spip_pipeline']['jqueryui_plugins'] .= "|insert_jqueryui";
// function insert_jqueryui($jqueryui_plugins) {
//    $jqueryui_plugins[] = "jquery.ui.dialog";
//    return $jqueryui_plugins ;
// }


/*
 * GESTION DES PARAMETRES DE DEVELOPPEMENT
 */
if( (strstr($_SERVER['HTTP_HOST'],'apsulis'))
	OR (strstr($_SERVER['HTTP_HOST'],'chox'))
	OR (strstr($_SERVER['HTTP_HOST'],'hady'))
	OR (strstr($_SERVER['HTTP_HOST'],'localhost'))
){
	define('_NO_CACHE',1);
	
	// Activer tous les logs
	define('_LOG_FILTRE_GRAVITE',8);
	
	// Activer un Debug Verbose sur SPIP
	error_reporting(E_ALL^E_NOTICE);
	ini_set ("display_errors", "On");
	define('SPIP_ERREUR_REPORT',E_ALL^E_NOTICE);
	define('SPIP_ERREUR_REPORT_INCLUDE_PLUGINS',E_ALL^E_NOTICE);

	ini_set('xdebug.max_nesting_level', 200);
	
	/* Activer le débug SQL pour optimiser le code */
	// define('_DEBUG_SLOW_QUERIES', true);
	// define('_BOUCLE_PROFILER', 500);
	// define('_LOG_FILELINE', true);
}else{
	// Serveur de production, on force le port
	// $_SERVER['SERVER_PORT']='443';
}

