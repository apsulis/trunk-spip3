<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function image_remplissage($p, $width = 800, $height= 600, $pos_recadre = 'center') {
	$p = filtrer('image_passe_partout',$p,$width,$height);
	$p = filtrer('image_recadre',$p,$width,$height,$pos_recadre);
	return $p;
}

function image_entiere($p, $width = 800, $height= 600) {
	$p = filtrer('image_reduire',$p,$width,$height);
	return $p;
}

/**
 * Enregistre une valeur dans le #ENV du squelette
 *
 * @example
 *     `[(#CALCUL|unccas_setenv{toto})]`
 *      enregistre le résultat de `#CALCUL`
 *      dans l'environnement toto et renvoie vide
 *
 *      `[(#CALCUL|unccas_setenv{toto,1})]`
 *      enregistre le résultat de `#CALCUL`
 *      dans la variable toto et renvoie la valeur
 *
 * @filtre
 *
 * @param array $Pile
 * @param mixed $val Valeur à enregistrer
 * @param mixed $key Nom de la variable
 * @param null|mixed $continue Si présent, retourne la valeur en sortie
 * @return string|mixed Retourne `$val` si `$continue` présent, sinon ''.
 */
function filtre_setenv(&$Pile, $val, $key, $continue = null) {
	$Pile[0][$key] = $val;
	return $continue ? $val : '';
}