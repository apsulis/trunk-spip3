<?php
/**
 * Définit les autorisations du plugin Configuration générale
 *
 * @plugin     Configuration générale
 * @copyright  2013
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Apsulis_config_generale\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/*
 * Un fichier d'autorisations permet de regrouper
 * les fonctions d'autorisations de votre plugin
 */

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function apsulis_config_generale_autoriser(){}


/* Exemple
function autoriser_configurer_apsulis_config_generale_dist($faire, $type, $id, $qui, $opt) {
	// type est un objet (la plupart du temps) ou une chose.
	// autoriser('configurer', '_apsulis_config_generale') => $type = 'apsulis_config_generale'
	// au choix
	return autoriser('webmestre', $type, $id, $qui, $opt); // seulement les webmestres
	return autoriser('configurer', '', $id, $qui, $opt); // seulement les administrateurs complets
	return $qui['statut'] == '0minirezo'; // seulement les administrateurs (même les restreints)
	// ...
}
*/



?>