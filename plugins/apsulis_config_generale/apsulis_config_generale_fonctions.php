<?php
/**
 * Fonctions utiles au plugin Configuration générale
 *
 * @plugin     Configuration générale
 * @copyright  2013
 * @author     Apsulis
 * @licence    GNU/GPL
 * @package    SPIP\Apsulis_config_generale\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

